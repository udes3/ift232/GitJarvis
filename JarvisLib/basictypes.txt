
!mute

#H�RITAGE: il manque la classe Object.
# Vous devriez la d�finir ici.
()
["set" _OperatorSetFunction]
42

!(Class new)
!ref Object


#MUTATEUR: c'est une fonctionnalit� 
#de base de tous les objets...


#Classe servant � faire des conditions
("true" "false")
[]
Object

!(Class new)
!ref ifelse

#OPERATIONSPRIMITIVES
#d�finition incompl�te de la classe int.
("value")
["+" _integerAddPrimitive "-" _integerSubtractPrimitive "*" _integerMultiplyPrimitive "==" _integerEqualsPrimitive "<" _integerLessPrimitive ">" _integerMorePrimitive]
Object

!(Class new)
!ref int

#OPERATIONSPRIMITIVES
#d�finition incompl�te de la classe bool.
("value")
["!" _boolNotPrimitive "&&" _boolAndPrimitive "||" _boolOrPrimitive]
Object

!(Class new)
!ref bool

#OPERATIONSPRIMITIVES
#d�finition de la classe bool.
("value")
["get" _listGetPrimitive]
Object

!(Class new)
!ref liste
