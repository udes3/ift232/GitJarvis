package jarvis.atoms.primitives;

import jarvis.atoms.AbstractAtom;
import jarvis.atoms.ListAtom;
import jarvis.atoms.ObjectAtom;
import jarvis.atoms.StringAtom;
import jarvis.interpreter.JarvisInterpreter;

public class OperatorSetFunction extends PrimitiveOperationAtom {

	
	protected void init() {
		argCount = 2;
	}
	
	
	@Override
	protected AbstractAtom execute(JarvisInterpreter ji,ObjectAtom self) {
		
		AbstractAtom var = (StringAtom) ji.getArg();
		AbstractAtom val = (AbstractAtom) ji.getArg();
		
		ListAtom parentAt = (ListAtom)self.getJarvisClass().message("attributes");
		int pos = parentAt.find(var);
		
		self.setValue(pos, val);
		return val;
	}

	@Override
	public String makeKey() {
		
		return "OperatorSetFunction";
	}
}
