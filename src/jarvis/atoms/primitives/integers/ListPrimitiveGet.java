package jarvis.atoms.primitives.integers;

import jarvis.atoms.AbstractAtom;
import jarvis.atoms.IntAtom;
import jarvis.atoms.ListAtom;
import jarvis.atoms.ObjectAtom;
import jarvis.atoms.primitives.PrimitiveOperationAtom;
import jarvis.interpreter.JarvisInterpreter;

public class ListPrimitiveGet extends PrimitiveOperationAtom {

	@Override
	protected AbstractAtom execute(JarvisInterpreter ji, ObjectAtom self) {

		// Ici, on peut assumer que l'objet qui a re�u le message (self) est un int et
		// poss�de donc
		// le champ "value".
		ListAtom data = (ListAtom) self.message("value");

		// Le second argument est pris de la file d'arguments. Il peut avoir n'importe
		// quelle forme.
		AbstractAtom arg2 = ji.getArg();
		IntAtom indice;
		
		if(arg2 instanceof IntAtom) {
			indice = (IntAtom)arg2;
			return data.get(indice.getValue());
		}
		//Si ce n'est pas un IntAtom ou un ObjectAtom, �a ne peut pas �tre le bon type d'argument.
		else throw new IllegalArgumentException(makeKey()+", argument 2: wrong atom type " + arg2.getClass().getName()+", value = "+arg2.makeKey());
		
	}

	@Override
	protected void init() {
		argCount = 1;

	}

	@Override
	public String makeKey() {

		return "ListPrimitiveGet";
	}

}
